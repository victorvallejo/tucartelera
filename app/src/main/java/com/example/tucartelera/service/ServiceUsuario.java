package com.example.tucartelera.service;

import android.content.Intent;
import android.util.Log;

import com.example.tucartelera.model.Usuario;

import io.realm.Realm;

public class ServiceUsuario {

    private final static int obtenerId(){
        Realm realm = Realm.getDefaultInstance();
        Number idAnterior = realm.where(Usuario.class).max("id");
        int idSiguiente;
        if (idAnterior == null){
            idSiguiente = 0;
        }else {
            idSiguiente = idAnterior.intValue()+1;
        }
        return idSiguiente;
    }

    public final static void agregarUsuario(final Usuario usuario){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction(){
            @Override
            public void execute(Realm realm){
                int id = ServiceUsuario.obtenerId();
                Usuario usuarioNuevo = realm.createObject(Usuario.class, id);
                usuarioNuevo.setNombre(usuario.getNombre());
                usuarioNuevo.setEmail(usuario.getEmail());
                usuarioNuevo.setTelefono(usuario.getTelefono());
                usuarioNuevo.setContraseña(usuario.getContraseña());
                Log.d("TAG", "Tu email es: " + usuario.getEmail() + " el nombre es: " + usuario.getNombre()
                        + " el telfono es: " + usuario.getTelefono() + " la contraseña es: " + usuario.getContraseña());
            }
        });
    }

    public final static Usuario buscarUsuarioPorCorreo(String email){
        Realm realm = Realm.getDefaultInstance();
        Usuario usuario = realm.where(Usuario.class).equalTo("email", email).findFirst();
        if (usuario == null){
            Log.d("TAG", "El e-mail que digitaste no se encuentra registrado");
        }else{
            Log.d("TAG", "Tu email registrado es: " + usuario.getEmail() + " el nombre es: " + usuario.getNombre()
                + " el telfono es: " + usuario.getTelefono() + " la contraseña es: " + usuario.getContraseña());
        }
        return usuario;
    }
}
