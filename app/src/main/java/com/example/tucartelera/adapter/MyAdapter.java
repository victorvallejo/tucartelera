package com.example.tucartelera.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.tucartelera.R;

public class MyAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    Context context;
    String[][] peliculas;
    int[] imgPeliculas;

    public MyAdapter(Context context, String[][] peliculas, int[] imgPeliculas) {
        this.context = context;
        this.peliculas = peliculas;
        this.imgPeliculas = imgPeliculas;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return imgPeliculas.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final View vista = inflater.inflate(R.layout.elemento_lista, null);

        TextView titulo = (TextView) vista.findViewById(R.id.titleMovie);
        TextView duracion = (TextView) vista.findViewById(R.id.duration);
        TextView director = (TextView) vista.findViewById(R.id.director);

        ImageView imagen = (ImageView) vista.findViewById(R.id.imagePelicula);
        RatingBar calificacion = (RatingBar) vista.findViewById(R.id.ratingPelicula);

        titulo.setText(peliculas[i][0]);
        director.setText(peliculas[i][1]);
        duracion.setText("Duración " + peliculas[i][2]);
        imagen.setImageResource(imgPeliculas[i]);
        calificacion.setProgress(Integer.valueOf(peliculas[i][3]));

        return vista;
    }
}
