package com.example.tucartelera.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.tucartelera.adapter.MyAdapter;
import com.example.tucartelera.R;

public class MovieActivity extends AppCompatActivity {

    ListView listaPeliculas;
    MenuItem cerrarSesion;

    String[][] peliculas = {
            {"1917", "Sam Mendes", "1h 59min", "8", "Primera Guerra Mundial, conocida en su momento como la Gran Guerra. En el frente occidental, el general británico Erinmore (Colin Firth) encomienda a los cabos Schofield (George MacKay) y Blake (Dean-Charles Chapman), dos jóvenes soldados británicos, una misión estrictamente imposible. Deberán entregar un mensaje urgente y decisivo al coronel MacKenzie (Benedict Cumberbatch). Para realizar esta misión deberán abandonar la trinchera a plena luz del día y avanzar por el campo francés ocupado por los alemanes. Sin respiro, en una carrera a contrarreloj, los dos soldados atravesarán angostas trincheras, alambradas y campo a través en un entorno de muerte y destrucción, rodeado de cadáveres de hombres y animales. Solo disponen de unas pocas horas para cumplir su cometido y evitar a toda costa un violento ataque. Si no llegan a tiempo, 1.600 soldados perderán la vida, entre ellos el hermano de uno de los dos jóvenes soldados."},
            {"Bad boys for life", "Adil El Arbi, Bilall Fallah", "2h 04min ", "9", "Los detectives Mike Lowrey (Will Smith) y Marcus Burnett (Martin Lawrence) vuelven a hacer de las suyas. Ahora Lowery pasa por la crisis de mediana edad y Burnett está pensando en retirarse. Eso sí, cuando les llega un nuevo caso que resolver, volverán a trabajar juntos por última vez. Esta vez, ante la nueva amenaza que se les presenta, tendrán que trabajar codo con codo con una moderna unidad policial que tendrá algunas diferencias con los dos detectives de la vieja escuela. ¿Seguirán siendo chicos malos de por vida? Esta tercera entrega de la saga Dos policías rebeldes (Bad Boys) la dirigen Adil El Arbi y Bilall Fallah, responsbales de las películas Gangsta (2018) y Black (2015). "},
            {"Le Mans '66", "James Mangold", "2h 33min", "10", "Son los años 60. El visionario diseñador de automóviles estadounidense Carroll Shelby (Matt Damon) y el intrépido piloto e ingeniero británico Ken Miles (Christian Bale) luchan contra las injerencias corporativas, además de contra las leyes de la física y sus propios demonios personales, con el objetivo de construir un revolucionario coche de carreras. Ese automóvil sería el Ford GT40 y su cometido sería desafiar al mismísimo Ferrari. El lugar: la mítica prueba de resistencia francesa de las 24 Horas de Le Mans en 1966."},
            {"Diamantes en bruto", " Benny Safdie, Josh Safdie", "2h 15min", "9", "Howard Ratner (Adam Sandler) es un joyero y padre de familia, carismático y embaucador, que tiene un cuestionable negocio de extravagantes joyas en la ciudad de Nueva York. Tanto en sus chanchullos como en su vida personal siempre asume el máximo riesgo, y por eso siempre está al borde de la bancarrota. Eso sí, está a punto de cerrar un jugoso negocio que involucra una apuesta de alto riesgo que podría hacerle millonario. Pero para hacer esa arriesgada apuesta tendrá que hacer equilibrismos entre sus negocios, su familia y los adversarios que le amenazan por todos los frentes. Este filme producido por Martin Scorsese lo dirigen los hermanos Josh y Benny Safdie, autores de las películas Good Time (2017) y Go Get Some Rosemary (2009). "},
            {"Historia de un matrimonio", "Noah Baumbach", "2h 17min", "9", "Nicole (Scarlett Johansson) es una actriz que dejó una prometedora carrera en el cine comercial para trabajar en la compañía teatral de su marido Charlie (Adam Driver), un director de teatro en pleno auge del que ahora se está divorciando. Con una química aplastante y un hijo en común, la historia de amor de esta pareja se romperá por completo, llegando a tener incluso que recurrir a los abogados y tribunales para zanjar una vida en común llena de heridas abiertas. El cineasta Noah Baumbach se encarga de dirigir y escribir el guion de este drama protagonizado por Adam Driver y Scarlett Johansson. "},
            {"El irlandés", "Martin Scorsese", "3h 29min", "8", "Frank Sheeran (Robert De Niro), más conocido como El irlandés, es un asesino a sueldo de la mafia al que se le atribuyen más de 25 asesinatos relacionados con el hampa. Al final de su vida, Sheeran afirmó haber estado involucrado en el asesinato de Jimmy Hoffa (Al Pacino), el poderoso jefe del sindicato de camioneros. Hoffa desapareció el 30 de julio 1975 y no fue declarado legalmente muerto hasta el 30 de julio de 1982. Su asesinato aún sigue siendo una incógnita y es uno de los misterios sin resolver más famosos en la historia de EE.UU."},
            {"Joker", "Todd Phillips", "2h 02min", "9", "Joker mostrará por primera vez los orígenes del icónico archienemigo por excelencia de Bruce Wayne/Batman. La historia sigue de cerca la vida de Arthur Fleck (Joaquin Phoenix), un hombre con problemas psiquiátricos que vivirá una serie de acontecimientos que le harán convertirse en uno de los grandes villanos de DC Comics. El Príncipe Payaso del Crimen se cruzará en el camino de Thomas Wayne (Brett Cullen) y se acercará a su hijo, el futuro Caballero Oscuro en su versión joven (Dante Pereira-Olson)."},
            {"Jojo rabit", "Taika Waititi", "1h 48min", "10", "Jojo Betzler (Roman Griffin Davis) es un solitario chico alemán que pertenece a las juventudes hitlerianas. Su mundo se pondrá patas arriba cuando descubre que su madre, Rosie (Scarlett Johansson), una mujer soltera, está escondiendo a Elsa (Thomasin McKenzie), una joven judía, en su propia casa. Ayudado por su amigo imaginario, Adolf Hitler (Taika Waititi), Jojo deberá afrontar su ciego nacionalismo con las contradicciones de una guerra absurda. Película basada en la novela de Christine Leunens, que dirige el también actor Taika Waititi. "},
            {"Parásitos", "Bong Joon Ho", " 2h 12min", "10", "Ki-taek (Kang-ho Song) es el patriarca de una familia pobre que malvive en un piso bajo en Seúl, pagando las facturas a base de trabajos precarios y robando el wi-fi de los vecinos. Su situación cambia un día en el que su hijo logra que le recomienden para dar clases particulares de inglés en casa de los Park, una familia acaudalada. Utilizando su ingenio, el joven conseguirá ganarse la confianza de la señora de la casa, y así irá introduciendo, poco a poco, al resto de sus familiares en distintos trabajos del servicio doméstico. Será el comienzo de un engranaje incontrolable, del cual nadie saldrá realmente indemne."},
    };

    int[] imgPeliculas = {R.drawable.guerra, R.drawable.bad, R.drawable.lemans, R.drawable.diamante, R.drawable.historia, R.drawable.irlandes, R.drawable.joker, R.drawable.jojo, R.drawable.parasitos};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listaPeliculas = (ListView) findViewById(R.id.listViewPeliculas);

        listaPeliculas.setAdapter(new MyAdapter(this, peliculas, imgPeliculas));

        listaPeliculas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent viewDescription = new Intent(view.getContext(), DescripcionPelicula.class);
                viewDescription.putExtra("IMG", imgPeliculas[position]);
                viewDescription.putExtra("TITLE", peliculas[position][0]);
                viewDescription.putExtra("DESCRIPTION", peliculas[position][4]);
                startActivity(viewDescription);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        cerrarSesion = (MenuItem) findViewById(R.id.close);
        Intent cerrarSes = new Intent(getApplicationContext(), Login.class);
        startActivity(cerrarSes);
        return super.onOptionsItemSelected(item);
    }
}