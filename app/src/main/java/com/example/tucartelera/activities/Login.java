package com.example.tucartelera.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tucartelera.R;
import com.example.tucartelera.model.Usuario;
import com.example.tucartelera.service.ServiceUsuario;

import io.realm.Realm;

public class Login extends AppCompatActivity {

    private EditText email, contraseña;
    private Button ingresarButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ingresarButton = findViewById(R.id.btn_ingresar);
        email = findViewById(R.id.email_login);
        contraseña = findViewById(R.id.password_login);

        ingresarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Usuario usuario;
                usuario = ServiceUsuario.buscarUsuarioPorCorreo(email.getText().toString());
                if (usuario == null){
                    Toast.makeText(getApplicationContext(), "El e-mail que digitaste no se encuentra registrado", Toast.LENGTH_LONG).show();
                }else if(usuario.getContraseña().equals(contraseña.getText().toString())){
                    Toast.makeText(getApplicationContext(), "Bienvenido de nuevo.", Toast.LENGTH_LONG).show();
                    irACartelera();
                }else {
                    Toast.makeText(getApplicationContext(), "La contraseña es incorrecta", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void irACartelera(){
        Intent intent = new Intent(getApplicationContext(), MovieActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}