package com.example.tucartelera.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tucartelera.R;

public class DescripcionPelicula extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion_pelicula);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageView img = (ImageView) findViewById(R.id.imgPeliculaDes);
        TextView title = (TextView) findViewById(R.id.titleMovieDes);
        TextView description = (TextView) findViewById(R.id.descriptionPelicula);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null){
            img.setImageResource(bundle.getInt("IMG"));
            title.setText(bundle.getString("TITLE"));
            description.setText(bundle.getString("DESCRIPTION"));
        }
    }
}