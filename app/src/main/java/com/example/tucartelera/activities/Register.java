package com.example.tucartelera.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tucartelera.R;
import com.example.tucartelera.service.ServiceUsuario;
import com.example.tucartelera.model.Usuario;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class Register extends AppCompatActivity {

    private EditText nombre, email, telefono, contraseña;
    private Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initRealm();

        register = findViewById(R.id.btn_register);
        nombre = findViewById(R.id.nombre_text);
        email = findViewById(R.id.email_register);
        telefono = findViewById(R.id.phone_text);
        contraseña = findViewById(R.id.password_text);


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Usuario usuario = new Usuario();
                usuario.setNombre(nombre.getText().toString());
                if (!emailValido(email.getText().toString())){
                    Toast.makeText(getApplicationContext(),"El email no tiene el formato correcto.", Toast.LENGTH_LONG).show();
                }else {
                    usuario.setEmail(email.getText().toString());
                }
                usuario.setTelefono(telefono.getText().toString());
                usuario.setContraseña(contraseña.getText().toString());
                Usuario usuarioValidacion = ServiceUsuario.buscarUsuarioPorCorreo(email.getText().toString());
                if (nombre.getText().toString().isEmpty() || email.getText().toString().isEmpty() || telefono.getText().toString().isEmpty() || contraseña.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Debes llenar todos los campos.", Toast.LENGTH_LONG).show();
                }else if(usuarioValidacion != null){
                    Toast.makeText(getApplicationContext(),"El email ya se encuentra registrado.", Toast.LENGTH_LONG).show();
                }else{
                    ServiceUsuario.agregarUsuario(usuario);
                    Toast.makeText(getApplicationContext(),"Te registraste de manera satisfactoria.", Toast.LENGTH_LONG).show();
                    irACartelera();
                }
            }
        });
    }

    private boolean emailValido(String email){
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void irACartelera(){
        Intent intent = new Intent(getApplicationContext(), MovieActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("Usuario")
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(configuration);
    }
}